from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Input required',
    }
    description_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Update your status here...'
    }
    # description_attrs = {
    #     'type': 'text',
    #     'cols': 50,
    #     'rows': 6,
    #     'class': 'status-form-textarea',
    #     'placeholder':'Update your status here...'
    # }

    description = forms.CharField(label='Update Status', required=True, max_length=250, widget=forms.TextInput(attrs=description_attrs))
    # description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))