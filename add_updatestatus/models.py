from django.db import models
# from django.core.validators import MaxLengthValidator
# Create your models here.

class Status(models.Model):
    description = models.CharField(max_length=250)
    #description = models.TextField(validators=[MaxLengthValidator(300)])
    created_date = models.DateTimeField(auto_now_add=True)