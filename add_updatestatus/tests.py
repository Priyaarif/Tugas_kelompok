from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status

# Create your tests here.
class UpdatestatusUnitTest(TestCase):
    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)

    def test_updatestatus_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(description='mengerjakan tugas_1 ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)
