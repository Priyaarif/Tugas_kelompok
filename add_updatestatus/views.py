from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}

def index(request):
    status = Status.objects.all()
    response['status_list'] = status
    html = 'status.html'
    response['update_status'] = Status_Form
    return render(request, html, response)

def update_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')
