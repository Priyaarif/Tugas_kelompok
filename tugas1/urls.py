"""tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
import add_profile.urls as profile
import add_friend.urls as add_friend
import add_stats.urls as stats
import add_updatestatus.urls as updatestatus
from add_friend.views import index as index_status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(profile, namespace="profile")),
    url(r'^add-friend/', include(add_friend, namespace="add-friend")),
    url(r'^stats/', include(stats, namespace="statistic")),
    url(r'^update-status/', include(updatestatus, namespace="update-status")),
    url(r'^$', index_status, name='index')
]
