from django.shortcuts import render
from .models import Identity

# Create your views here.
response = {'author': "DiligentSquad"}
def index(request):
    if Identity.objects.all().count()==0:
        Identity.objects.create(
        name = "Kitty",
        birthday = "1996-1-11",
        gender = "Female",
        expertise1 = "Statistics",
        expertise2 = "Programming",
        expertise3 = "Public Speaking",
        email = "blahblahblah@gmail.com",
        description = "Give me fish"
        )
    identity = Identity.objects.get(id=1)
    response = {
    'name':identity.name,
    'gender':identity.gender,
    'birthday':identity.birthday,
    'email':identity.email,
    'expertise1':identity.expertise1,
    'expertise2':identity.expertise2,
    'expertise3':identity.expertise3,
    'description':identity.description,
    }
    html = "profile.html"
    return render(request, html, response)
