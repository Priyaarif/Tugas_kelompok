from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Identity

# Create your tests here.
class ProfileTest(TestCase):

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_model_can_create_insertion(self):
        Identity.objects.create(
        name = "Kitty",
        birthday = "1996-1-11",
        gender = "Female",
        expertise1 = "Statistics",
        expertise2 = "Programming",
        expertise3 = "Public Speaking",
        email = "blahblahblah@gmail.com",
        description = "Give me fish"
        )
        self.assertEqual(Identity.objects.all().count(), 1)
