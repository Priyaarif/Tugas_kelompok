'''
from django import forms

class EditProfileFormLeft(forms.Form):
    error_friends = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan URL media sosial',
    }
    attrs = {
        'class': 'form-control'
    }

    Name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    Birthday = forms.DateField(label="Birthday", required=True, widget=forms.DateInput(attrs=attrs))
    Gender = forms.TypedChoiceField(label="Gender", required=True, widget=forms.Select(attrs=attrs))
    Expertise = forms.ChoiceField(label="Expertise", required=True, widget=forms.Select(attrs=attrs))
    Email = forms.EmailField(label="Email", required=True, widget=forms.EmailInput(attrs=attrs))
    ImageSource = forms.URLField(label='URL', required=False, widget=forms.URLInput(attrs=attrs))

class EditProfileFormRight(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    forms.CharField(label='Description', required=False, widget=forms.Textarea(attrs=description_attrs))
'''
