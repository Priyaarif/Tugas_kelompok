from django.db import models

# Create your models here.
class Identity(models.Model):
    name = models.CharField(max_length=27, null = 'True')
    birthday = models.DateField(null = 'True')
    gender = models.TextField(null = 'True')
    expertise1 = models.TextField(null = 'True')
    expertise2 = models.TextField(null = 'True')
    expertise3 = models.TextField(null = 'True')
    email = models.EmailField(null = 'True')
    description = models.TextField(null = 'True')
