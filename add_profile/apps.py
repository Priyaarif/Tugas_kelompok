from django.apps import AppConfig


class AddProfileConfig(AppConfig):
    name = 'add_profile'
