from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from add_profile.models import Identity
from add_friend.models import Friend
from add_updatestatus.models import Status
from django.http import HttpRequest

# Create your tests here.
class StatsUnitTest(TestCase):

    def test_stats_is_exist(self):
        make_profile = Identity.objects.create(
        name = 'Priya',
        gender = 'Male',
        birthday = '1996-06-15',
        email = 'test@gmail.com',
        expertise1 = 'dnea',
        expertise2 = 'dnea',
        expertise3 = 'dnea',
        description = 'frknd',
        )
        make_status = Status.objects.create(
        description='Aku lagi galau',
        )
        make_friend = Friend.objects.create(
        Name = 'Priya',
        URL = 'http://test.com'
        )
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)
