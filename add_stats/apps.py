from django.apps import AppConfig


class AddStatsConfig(AppConfig):
    name = 'add_stats'
