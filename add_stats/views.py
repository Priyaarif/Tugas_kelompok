from django.shortcuts import render
from add_profile.models import Identity
from add_friend.models import Friend
from add_updatestatus.models import Status


response = {'author': "DiligentSquad"}
def index(request):
	profile = Identity.objects.get(id=1)
	response['name'] = profile.name
	html = 'stats.html'
	response['friends'] = Friend.objects.all().count()
	response['feed'] = Status.objects.all().count()
	response['latestpost'] = Status.objects.latest('id')
	return render(request, html, response)
