from django.db import models

class Friend(models.Model):
    Name = models.CharField(max_length=27)
    URL = models.URLField()
    created_date = models.DateTimeField(auto_now_add=True)