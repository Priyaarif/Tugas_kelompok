from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

# Create your views here.
response = {}
def index(request):
    html = 'add_friend/add_friend.html'
    response['friend_form'] = Friend_Form
    response['friend'] = Friend.objects.all().order_by('created_date')
    return render (request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['Name'] = request.POST['Name']
        response['URL'] = request.POST['URL']
        friend=Friend(Name=response['Name'], URL=response['URL'])
        friend.save()
        return HttpResponseRedirect('/add-friend/')
    else:
        return HttpResponseRedirect('/add-friend/')
