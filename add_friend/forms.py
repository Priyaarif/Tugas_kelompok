from django import forms

class Friend_Form(forms.Form):
    error_friends = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan URL media sosial',
    }
    attrs = {
        'class': 'form-control'
    }

    Name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    URL = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=attrs))
