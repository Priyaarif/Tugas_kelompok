from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Friend
from .forms import Friend_Form

class AddfriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_addfriend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_model_can_add_new_friend(self):
        # Creating a new activity
        new_friend = Friend.objects.create(Name='Annisa', URL='twitter.com/nisasrdv')

        # Retrieving all available activity
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_form_friend_input_has_placeholder_and_css_classes(self):
        form = Friend_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('<label for="id_Name">Name:</label>', form.as_p())
        self.assertIn('<label for="id_URL">URL:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'Name': '', 'URL': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['URL'],
            ["This field is required."]
        )
    def test_addfriend_post_fail(self):
        response = Client().post('/add-friend/add_friend', {'Name': 'Anonymous', 'URL': ''})
        self.assertEqual(response.status_code, 302)